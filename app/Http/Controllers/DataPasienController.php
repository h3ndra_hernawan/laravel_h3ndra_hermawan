<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\DataPasien;
use PDF;

class datapasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datarumahsakit = DB::table('datarumahsakit')->get();   
        
        if (session()->get('level') == 1){
            $datapasien = DB::table('datapasien')
            ->join('datarumahsakit', 'datapasien.id_rumah_sakit', '=', 'datarumahsakit.id')
            ->select('datapasien.id','datapasien.nama_pasien','datapasien.alamat','datapasien.no_tlp', 'datapasien.id_rumah_sakit','datapasien.nama_user','datarumahsakit.nama_rumah_sakit')
            ->get(); 
        }else{
            $datapasien = DB::table('datapasien')
            ->join('datarumahsakit', 'datapasien.id_rumah_sakit', '=', 'datarumahsakit.id')
            ->select('datapasien.id','datapasien.nama_pasien','datapasien.alamat','datapasien.no_tlp', 'datapasien.id_rumah_sakit','datapasien.nama_user','datarumahsakit.nama_rumah_sakit')
            ->where('nama_user',session()->get('nama') )
            ->get(); 
        }
   
        $page = 'datapasien';
        return view('datapasien.index',compact('page','datapasien','datarumahsakit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function id_rumah_sakit(Request $request)
    {
        $data_rumah_sakit = DB::table('datarumahsakit')->get(); 
        return $data_rumah_sakit;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama_pasien' => 'required',
            'alamat' => ['required', 'max:255'],
            'no_tlp' => ['required', 'max:12'],
            'id_rumah_sakit' => 'required'
        ]);
        $DataPasien  =  DB::table('datapasien')->insert([
            'nama_pasien' => $request->nama_pasien,
            'alamat' => $request->alamat,
            'no_tlp' => $request->no_tlp,
            'id_rumah_sakit' => $request->id_rumah_sakit,
            'nama_user' => session()->get('nama') ,
            'created_at' => new \DateTime()
        ]);
        if(!is_null($DataPasien)) {            
            return redirect('DataPasien')->with('success' , 'Data Sukses Tersimpan');
        }    
        else {
            return redirect('DataPasien')->with('gagal' , 'Data Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $DataPasien = DB::table('datapasien')
        ->where('id',$id)->get();   
    return $DataPasien;    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print_pdf($id)
    {
        
        $datapasien = DB::table('datapasien')
            ->join('datarumahsakit', 'datapasien.id_rumah_sakit', '=', 'datarumahsakit.id')
            ->select('datapasien.id','datapasien.nama_pasien','datapasien.alamat','datapasien.no_tlp', 'datapasien.id_rumah_sakit','datapasien.nama_user','datarumahsakit.nama_rumah_sakit')
            ->where('datapasien.id', '=',$id)
            ->take(1)
            ->get(); 
        $pdf = PDF::loadView('DataPasien.print_pdf', compact('datapasien'))->setPaper('A4', 'landscape');
        return $pdf->stream('Data_pasien.pdf');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            'nama_pasien' => 'required',
            'alamat' => ['required', 'max:255'],
            'no_tlp' => ['required', 'max:12'],
            'id_rumah_sakit' => 'required'
        ]);
        $tgl_lahir =  date('Y-m-d', strtotime(($request->tgl_lahir)));
        $DataPasien = DB::table('datapasien')->where('id',$request->id_pasien)->update([
            'nama_pasien' => $request->nama_pasien,
            'alamat' => $request->alamat,
            'no_tlp' => $request->no_tlp,
            'id_rumah_sakit' => $request->id_rumah_sakit,
            'nama_user' => session()->get('nama'),
            'updated_at' => new \DateTime()
        ]);
        if(!is_null($DataPasien)) {            
            return redirect('DataPasien')->with('success' , 'Data Sukses diperbaharui');
        }    
        else {
            return redirect('DataPasien')->with('gagal' , 'Data Gagal diperbaharui');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $level = DB::table('datapasien')->where('id',$request->id_rs)->delete();

        if(!is_null($level)) {       
            return response()->json([
                'success' => true,
                'message' => 'Data Sukses Di Hapus'
            ], 200);
        }    
        else {
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Di Hapus'
            ], 401);
        }
    }
}
