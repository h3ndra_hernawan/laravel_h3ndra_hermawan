<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class DataPasien extends Authenticatable
{
    use Notifiable;
    // public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $table = 'users';
    protected $fillable = [
    'nama_pasien','alamat','tlp','id_rumah_sakit','nama_user'];
}
