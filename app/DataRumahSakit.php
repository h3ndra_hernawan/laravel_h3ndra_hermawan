<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class DataRumahSakit extends Authenticatable
{
    use Notifiable;
    // public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $table = 'users';
    protected $fillable = [
        'nama_rumah_sakit','alamat','email','no_tlp'];
}
