<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\DataRumahSakit;

class DataRumahSakitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_rumah_sakit = DB::table('datarumahsakit')->get(); 
        $page = 'data_rumah_sakit';
        return view('DataRumahSakit.index',compact('page','data_rumah_sakit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama_rumah_sakit' => 'required|unique:DataRumahSakit',
            'alamat' => ['required', 'max:255'],
            'no_tlp' => ['required', 'max:12'],
            'email' => 'required'
        ]);
        $data_rumah_sakit  =  DB::table('datarumahsakit')->insert([
            'nama_rumah_sakit' => $request->nama_rumah_sakit,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'no_tlp' => (integer)$request->no_tlp,
            'created_at' => new \DateTime()
        ]);
        if(!is_null($data_rumah_sakit)) {            
            return redirect('data_rumah_sakit')->with('success' , 'Data Sukses Tersimpan');
        }    
        else {
            return redirect('data_rumah_sakit')->with('gagal' , 'Data Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datarumahsakit = DB::table('datarumahsakit')
        ->where('id',$id)->get();   
        return $datarumahsakit;  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    //    dd($request->all());
       $request->validate([
        'nama_rumah_sakit' => 'required',
        'alamat' => ['required', 'max:255'],
        'no_tlp' => ['required', 'max:12'],
        'email' => 'required'
    ]);
    $data_rumah_sakit = DB::table('datarumahsakit')->where('id',$request->rs_id_edit)->update([
        'nama_rumah_sakit' => $request->nama_rumah_sakit,
        'alamat' => $request->alamat,
        'email' => $request->email,
        'no_tlp' => (integer)$request->no_tlp,
        'updated_at' => new \DateTime()
    ]);
    if(!is_null($data_rumah_sakit)) {            
        return redirect('data_rumah_sakit')->with('success' , 'Data Sukses diperbaharui');
    }    
    else {
        return redirect('data_rumah_sakit')->with('gagal' , 'Data Gagal diperbaharui');
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $level = DB::table('datarumahsakit')->where('id',$request->id_rs)->delete();

        if(!is_null($level)) {       
            return response()->json([
                'success' => true,
                'message' => 'Data Sukses Di Hapus'
            ], 200);
        }    
        else {
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Di Hapus'
            ], 401);
        }
    }
}
