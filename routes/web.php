<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', 'LoginController@index');
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\NotificationController;


Route::group(['prefix' => '/' , 'middleware' => 'web'], function () {
    Route::get('/', 'LoginController@index')->name('/');
    Route::post('/login.check_login', 'LoginController@postlogin')->name('login.check_login');  
  
}); 
Route::group(['prefix'=>'/','middleware' => 'check'], function () {
    Route::post('logout' , '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
    Route::get('dashboard', 'DashboardController@index')->name('/dashboard');
    Route::get('/user', 'UserController@index')->name('/user');

    Route::post('/data_rumah_sakit.delete', 'DataRumahSakitController@destroy')->name('data_rumah_sakit.delete'); 
    Route::post('/DataPasien.delete', 'DataPasienController@destroy')->name('DataPasien.delete'); 

    Route::get('/data/id_rumah_sakit' , 'DataPasienController@id_rumah_sakit');
    Route::get('/print_pdf/{id}','DataPasienController@print_pdf')->name('/print_pdf');
    


    Route::resource('user', 'UserController');
    Route::resource('data_rumah_sakit', 'DataRumahSakitController');
    Route::resource('DataPasien', 'DataPasienController');
}); 