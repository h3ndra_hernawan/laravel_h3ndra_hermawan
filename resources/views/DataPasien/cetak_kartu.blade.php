<!DOCTYPE html>
<html>
<head>
<style>
    .button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
@media print{
   .button2{
       display:none;
   }
}
.button2 {background-color: #008CBA;} /* Blue */
</style>
</style>
</head>
<body>
<table width="100%" border="1">
    <thead>
        @foreach ($datapasien as $item)
        <tr>
            <th>id Pasien:</th><td>{{ $item->id_pasien }}</td>
        </tr>
        <tr>
            <th>Nama Pasien:</th><td>{{ $item->nama_pasien }}</td>
        </tr>
        <tr>
            <th>Alamat:</th><td>{{ $item->alamat }}</td>
        </tr>
        <tr>
            <th>no Tlp:</th><td>{{ $item->no_tlp }}</td>
        </tr>
        <tr>
            <th>Rt / Rw:</th><td>{{ $item->rt }} / {{ $item->rw }}</td>
        </tr>
        <tr>
            <th>Nama Kelurahan:</th><td>{{ $item->nama_kelurahan }}</td>
        </tr>
        <tr>
            <th>Tgl Lahir:</th><td>{{ $item->tgl_lahir }}</td>
        </tr>
        <tr>
            <th>Jenis Kelamin:</th><td>{{ $item->jenis_kelamin }}</td>
        </tr>
        @endforeach
    </thead>
</table>
<a href="{{ url('/DataPasien') }}" class="button button2" >Kembali</a>
<script type="text/javascript">
    window.print();
    window.open('".$url."', '_blank')
</script>
</body>
</html>