@extends('layouts.app')
@section('content')
    <div class="main-content-inner">
        <!-- sales report area start -->
        <div class="sales-report-area mt-5 mb-5">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#btn-add"><i class="ti-plus"></i>
                        &nbsp; Tambah Data Pasien
                        </button>
                        @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="p-0 m-0" style="list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="data-tables">
                            <table id="dataTable" class="text-center" width="100%">
                                <thead class="bg-light text-capitalize">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Pasien</th>
                                        <th>Alamat</th>
                                        <th>no Tlp</th>
                                        <th>Nama Rumah Sakit</th>
                                        @if (session()->get('level') == 1)
                                            <th>User Input</th>
                                        @endif
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $counter = 0
                                    @endphp
                                    @foreach ($datapasien as $item)
                                    <tr>
                                        <td>{{ $counter += 1 }}</td>
                                        <td>{{ $item->nama_pasien }}</td>
                                        <td>{{ $item->alamat }}</td>
                                        <td>{{ $item->no_tlp }}</td>
                                        <td>{{ $item->nama_rumah_sakit }}</td>
                                        @if (session()->get('level') == 1)
                                            <td>{{ $item->nama_user }}</td>
                                        @endif
                                        <td>
                                            <div style="white-space: nowrap;">
                                                <a href="{{ url('/print_pdf/'.$item->id) }}" class="btn btn-primary mb-3"><span class="fa fa-file-pdf-o"> Print Pdf</a>
                                                <button type="button" class="btn btn-warning mb-3 edit" data-toggle="modal" data-target="#btn-edit"
                                                id="{{ $item->id }}"><i class="ti-pencil"></i> Edit</button>
                                                <button type="submit" class="btn btn-danger mb-3 hapus" id="{{ $item->id }}" ><i class="ti-trash"></i> Hapus</button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Modal Tambah-->
                    <div class="modal fade" id="btn-add">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <i class="ti-plus"></i>
                                    &nbsp; &nbsp; <h5 class="modal-title">Tambah Data Pasien</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form action="{{ url('/DataPasien') }}" method="POST">
                                        @csrf
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="nama_pasien">Nama Pasien<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('nama_pasien') is-invalid @enderror" id="nama_pasien" name="nama_pasien" type="text" value="{{old('nama_pasien')}}"required>
                                                        @if($errors->has('nama_pasien'))
                                                            <span class="alert-message">{{$errors->first('nama_pasien')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat<sup style="color: red">*</sup></label>
                                                        <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" id="alamat" cols="6" rows="6" value="{{old('alamat')}}"required></textarea>
                                                        @if($errors->has('alamat'))
                                                            <span class="alert-message">{{$errors->first('alamat')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="no_tlp">No Tlp<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('no_tlp') is-invalid @enderror" id="no_tlp" type="number" name="no_tlp" value="{{old('no_tlp')}}"required>
                                                        
                                                        <sup style="color: red">Max 12 karekter</sup>
                                                        @if($errors->has('no_tlp'))
                                                            <span class="alert-message">{{$errors->first('no_tlp')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="id_rumah_sakit">Nama Rumah Sakit<sup style="color: red">*</sup></label>
                                                        <select class="form-control select form-input-select" data-fouc id="id_rumah_sakit"
                                                            name="id_rumah_sakit" required>
                                                        </select>
                                                        @if($errors->has('id_rumah_sakit'))
                                                            <span class="alert-message">{{$errors->first('id_rumah_sakit')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Edit-->
                    <div class="modal fade" id="btn-edit">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <i class="ti-plus"></i>
                                    &nbsp; &nbsp; <h5 class="modal-title">Edit Rumah Sakit</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form id="form-edit" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="card-body">
                                                <div class="row">
                                                    <input type="hidden" id="edit_id_pasien" class="form-control" name="id_pasien">
                                                    <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="nama_pasien">Nama Pasien<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('nama_pasien') is-invalid @enderror" id="edit_nama_pasien" name="nama_pasien" type="text" value="{{old('nama_pasien')}}"required>
                                                        @if($errors->has('nama_pasien'))
                                                            <span class="alert-message">{{$errors->first('nama_pasien')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat<sup style="color: red">*</sup></label>
                                                        <textarea class="form-control @error('alamat') is-invalid @enderror" name="alamat" id="edit_alamat" cols="6" rows="6" value="{{old('alamat')}}"required></textarea>
                                                        @if($errors->has('alamat'))
                                                            <span class="alert-message">{{$errors->first('alamat')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="no_tlp">No Tlp<sup style="color: red">*</sup></label>
                                                        <input class="form-control @error('no_tlp') is-invalid @enderror" id="edit_no_tlp" type="number" name="no_tlp" value="{{old('no_tlp')}}"required>
                                                        
                                                        <sup style="color: red">Max 12 karekter</sup>
                                                        @if($errors->has('no_tlp'))
                                                            <span class="alert-message">{{$errors->first('no_tlp')}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="id_rumah_sakit">Nama Rumah Sakit<sup style="color: red">*</sup></label>
                                                        <select class="form-control select form-input-select" data-fouc id="edit_id_rumah_sakit"
                                                            name="id_rumah_sakit" required>
                                                            @foreach ($datarumahsakit as $item)
                                                                <option value="{{ $item->id }}">{{ $item->nama_rumah_sakit}}</option>
                                                                @endforeach
                                                        </select>
                                                        @if($errors->has('id_rumah_sakit'))
                                                            <span class="alert-message">{{$errors->first('id_rumah_sakit')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- overview area end -->
    </div>
@endsection
@section('javascript')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script> 
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

@if( $massage = session('success'))
    <script type="text/javascript">
    Swal.fire({
        type: 'success',
        title:  "{{ $massage }}",
        text: 'success',
        timer: 4000,
    })
    </script>   
@endif
@if ( $massage = session('gagal'))
    <script type="text/javascript">
    Swal.fire({
        type: 'error',
        title:  "{{ $massage }}",
        text: 'Gagal',
        timer: 4000,
    })
@endif 
<script type="text/javascript">
    $(document).ready(function () { 
        var select_level = document.getElementById('id_rumah_sakit')
        $('#id_rumah_sakit option').remove()
        var option = document.createElement('option')
        option.value = 0
        option.text = "Pilih Nama Rumah Sakit"
        option.selected = true
        select_level.add(option)
        
        $.ajax({
            url: "{{ url('/data/id_rumah_sakit') }}",
            success: function (result) {
                result.forEach(element => {
                    var option = document.createElement('option')
                    option.value = element.id
                    option.text = element.nama_rumah_sakit
                    select_level.add(option)
                });
            }
        })
        $('#id_rumah_sakit').select2({
            theme: "classic",
            width: '100%',
            allowClear: true
        });
            
        $('#edit_id_rumah_sakit').select2({
            theme: "classic",
            width: '100%',
            allowClear: true
        });
    });
       $(document).on('click', '.edit', function(){  
            var id_pasien= $(this).attr('id')
            $('#form-edit').attr('action', '/DataPasien/' + id_pasien+ '')
            document.getElementById('edit_id_pasien').value = ''
            document.getElementById('edit_nama_pasien').value = ''
            document.getElementById('edit_alamat').value = ''
            document.getElementById('edit_no_tlp').value = ''
            document.getElementById('edit_id_rumah_sakit').value = ''
        

            $.ajax({
                url: '/DataPasien/' + id_pasien,
                success: function (res) {
                    console.log(res);
                        document.getElementById('edit_id_pasien').value = res[0].id
                        document.getElementById('edit_alamat').value = res[0].alamat
                        document.getElementById('edit_no_tlp').value = res[0].no_tlp
                        document.getElementById('edit_nama_pasien').value = res[0].nama_pasien
                        document.getElementById('edit_id_rumah_sakit').value = res[0].id_rumah_sakit
                        $('#btn-edit').modal('show')
                }
        })

    });
    $('body').on('click', '.hapus', function () {
        var token = $("meta[name='csrf-token']").attr("content");
        var id_rs = $(this).attr('id')
        let text = "return confirm('Apakah yakin data akan Di Delete ?";
        if (confirm(text) == true) {
            $.ajax({
                url: "{{ route('DataPasien.delete') }}",
                type: "POST",
                dataType: "JSON",
                cache: false,
                data: {
                    "id_rs": id_rs,
                    "_token": token
                },success:function(response){
                  console.log('cek',response);
                    if (response.success) {
                          
                        Swal.fire({
                            type: 'success',
                            title:  response.message,
                            text: 'success',
                            timer: 4000,
                        }).then (function() {
                                window.location = '{{ url('DataPasien') }}';
                            });

                    } else if (response.false){
                        console.log(response.success);

                        Swal.fire({
                            type: 'error',
                            title: response.message,
                            text: 'silahkan coba lagi!'
                        });

                    }

                    console.log(response);

                },

                error:function(response){
                  console.log('cek2',response);
                  Swal.fire({
                        type: 'error',
                        title: response.responseJSON.message,
                        text: 'silahkan coba lagi!'
                    });
                }

            });
        } 
    });
    var no_tlp = document.getElementById('no_tlp');
        no_tlp.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(no_tlp.value.length >12){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 12 Karekter",
                    timer: 4000,
                })
                no_tlp.value = no_tlp.value.slice(0, 12);
            }
        });
        var alamat = document.getElementById('alamat');
        alamat.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(alamat.value.length >255){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 255 Karekter",
                    timer: 4000,
                })
                alamat.value = alamat.value.slice(0, 255);
            }
        });

        var edit_no_tlp = document.getElementById('edit_no_tlp');
        edit_no_tlp.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(edit_no_tlp.value.length >12){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 12 Karekter",
                    timer: 4000,
                })
                edit_no_tlp.value = edit_no_tlp.value.slice(0, 12);
            }
        });
        var edit_alamat = document.getElementById('edit_alamat');
        edit_alamat.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            if(alamat.value.length >255){
                Swal.fire({
                    type: 'warning',
                    title:  "Maksimal 255 Karekter",
                    timer: 4000,
                })
                edit_alamat.value = edit_alamat.value.slice(0, 255);
            }
        });
       
</script>
@endsection