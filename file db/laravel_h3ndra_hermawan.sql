-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Jan 2023 pada 20.25
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_h3ndra_hermawan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `datapasien`
--

CREATE TABLE `datapasien` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_pasien` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_tlp` bigint(11) NOT NULL,
  `id_rumah_sakit` int(11) NOT NULL,
  `nama_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `datapasien`
--

INSERT INTO `datapasien` (`id`, `nama_pasien`, `alamat`, `no_tlp`, `id_rumah_sakit`, `nama_user`, `created_at`, `updated_at`) VALUES
(1, 'Hendra', 'jla', 1111111111, 0, 'Hendra Hermawan', '2023-01-28 10:59:08', NULL),
(2, 'Hendra 2', 'ffff', 33333, 0, 'Hendra Hermawan', '2023-01-28 10:59:47', NULL),
(3, 'Hendra 2', 'sss', 11111, 10, 'Hendra Hermawan', '2023-01-28 11:45:34', '2023-01-28 12:07:42'),
(5, 'ema', 'jl', 1234444, 9, 'Hendra Hermawan', '2023-01-28 12:17:48', NULL),
(6, 'Hendra 5', 'dddd', 34444, 12, 'ema', '2023-01-28 12:22:26', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `datarumahsakit`
--

CREATE TABLE `datarumahsakit` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_rumah_sakit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_tlp` bigint(12) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `datarumahsakit`
--

INSERT INTO `datarumahsakit` (`id`, `nama_rumah_sakit`, `alamat`, `email`, `no_tlp`, `created_at`, `updated_at`) VALUES
(9, 'Rs al islam', 'jl sukarno hata', 'alislam@com', 12330877, '2023-01-28 09:59:28', NULL),
(10, 'Rs Boromeus', 'jl', 'admin@aulia.id', 12345, '2023-01-28 10:20:10', NULL),
(12, 'Rs Hasa Sadikin', 'jl bandung', 'rt@y.com', 222222, '2023-01-28 12:20:14', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(12) NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama`, `user`, `password`, `level`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Hendra Hermawan', 'admin', '$2y$10$bxuq3111B/S7iDvHjcwfQuSR5gKnFwsKevnZJ77ETAUu9zMtgo4EO', '1', 1, '2023-01-21 19:12:27', NULL),
(2, 'Hendra', 'operator', '$2y$10$0yklJJmn2/O8qmZg0N0P5eLKEkJRbT/ldxoRPNiqgGau8pjPVL1eO', '2', 1, '2023-01-21 19:12:54', '2023-01-21 20:31:20'),
(4, 'ema', 'ema', '$2y$10$B00ujMv1gj05HIL/HpWESegBQ7oJ3Z1z1TgRqkHKA8LV3imk7i0QW', '2', 1, '2023-01-28 07:59:48', '2023-01-28 12:21:26');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `datapasien`
--
ALTER TABLE `datapasien`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `datarumahsakit`
--
ALTER TABLE `datarumahsakit`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`,`user`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `datapasien`
--
ALTER TABLE `datapasien`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `datarumahsakit`
--
ALTER TABLE `datarumahsakit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
